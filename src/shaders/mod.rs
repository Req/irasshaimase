
pub mod basic;

use std::fmt::{
	self,
	Display,
	Formatter
};
use web_sys::{
	WebGlProgram,
	WebGlShader
};
use crate::engine::GL;

pub struct ShaderPair {
	fragment: &'static str,
	vertex: &'static str
}

#[repr(u32)]
#[derive(Clone, Copy)]
enum ShaderType {
	Fragment = GL::FRAGMENT_SHADER,
	Vertex = GL::VERTEX_SHADER
}

impl Display for ShaderType {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{}", match self {
			Self::Fragment => "fragment",
			Self::Vertex => "vertex"
		})
	}
}

fn compile_impl(context: &GL, src: &str, stype: ShaderType) -> Result<WebGlShader, String> {
	let shader = context.create_shader(stype as u32).ok_or_else(|| String::from("Unable to create shader object"))?;
	context.shader_source(&shader, src);
	context.compile_shader(&shader);

	if context.get_shader_parameter(&shader, GL::COMPILE_STATUS).as_bool().unwrap_or(false) {
		Ok(shader)
	} else {
		Err(context.get_shader_info_log(&shader).unwrap_or_else(|| String::from("Unknown error")))
	}
}

fn compile(context: &GL, src: &str, stype: ShaderType) -> Result<WebGlShader, String> {
	compile_impl(context, src, stype).map_err(|e| format!("When compiling {} shader:\n{}", stype, e))
}

impl ShaderPair {
	pub fn link(&self, context: &GL) -> Result<WebGlProgram, String> {
		let fragment = compile(context, self.fragment, ShaderType::Fragment)?;
		let vertex = compile(context, self.vertex, ShaderType::Vertex)?;
		let program = context.create_program().ok_or_else(|| String::from("Unable to create program object"))?;
		context.attach_shader(&program, &fragment);
		context.attach_shader(&program, &vertex);
		context.link_program(&program);
		if context.get_program_parameter(&program, GL::LINK_STATUS).as_bool().unwrap_or(false) {
			Ok(program)
		} else {
			Err(context.get_program_info_log(&program).unwrap_or_else(|| String::from("Unknown error when linking shader")))
		}
	}
}
