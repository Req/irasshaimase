
use web_sys::{
	WebGlProgram,
	WebGlUniformLocation
};
use crate::{
	engine::GL,
	shaders::ShaderPair,
	wrappers::WebExpect
};

macro_rules! uniform {
	($gl: expr, $program: expr, $name: expr) => {
		$gl.get_uniform_location($program, $name).expect_log(concat!("Cannot find ", $name, " uniform"))
	}
}

macro_rules! uniform_index {
	($gl: expr, $program: expr, $array: expr, $index: expr, $name: expr) => {{
		let id = format!("{}[{}].{}", $array, $index, $name);
		$gl.get_uniform_location(&$program, &id).expect_log_f(|| format!("Cannot find {} uniform", id))
	}}
}

macro_rules! attrib {
	($gl: expr, $program: expr, $name: expr) => {
		$gl.get_attrib_location(&$program, $name).try_into().unwrap()
	}
}

const SELF: ShaderPair = ShaderPair {
	fragment: include_str!("basic.fragment.glsl"),
	vertex: include_str!("basic.vertex.glsl")
};

pub const POINT_LIGHT_LIMIT: usize = 10;
pub const SPOT_LIGHT_LIMIT: usize = 10;

pub struct Attribs {
	pub position: u32,
	pub color: u32,
	pub normal: u32
}

pub struct MaterialUniforms {
	pub ambient: WebGlUniformLocation,
	pub diffuse: WebGlUniformLocation,
	pub specular: WebGlUniformLocation,
	pub shininess: WebGlUniformLocation,
}

impl MaterialUniforms {
	fn new(gl: &GL, program: &WebGlProgram) -> Self {
		Self {
			ambient: uniform!(gl, program, "material.ambient"),
			diffuse: uniform!(gl, program, "material.diffuse"),
			specular: uniform!(gl, program, "material.specular"),
			shininess: uniform!(gl, program, "material.shininess")
		}
	}
}

pub struct DirectionalLightUniforms {
	pub direction: WebGlUniformLocation,

	pub color: WebGlUniformLocation,
	pub ambient: WebGlUniformLocation,
	pub diffuse: WebGlUniformLocation,
	pub specular: WebGlUniformLocation
}

impl DirectionalLightUniforms {
	fn new(gl: &GL, program: &WebGlProgram) -> Self {
		Self {
			direction: uniform!(gl, program, "directional_light.direction"),
			color: uniform!(gl, program, "directional_light.color"),
			ambient: uniform!(gl, program, "directional_light.ambient"),
			diffuse: uniform!(gl, program, "directional_light.diffuse"),
			specular: uniform!(gl, program, "directional_light.specular"),
		}
	}
}

pub struct PointLightUniforms {
	pub position: WebGlUniformLocation,

	pub color: WebGlUniformLocation,
	pub ambient: WebGlUniformLocation,
	pub diffuse: WebGlUniformLocation,
	pub specular: WebGlUniformLocation,

	pub constant: WebGlUniformLocation,
	pub linear: WebGlUniformLocation,
	pub quadratic: WebGlUniformLocation
}

impl PointLightUniforms {
	fn new(gl: &GL, program: &WebGlProgram, index: usize) -> Self {
		Self {
			position: uniform_index!(gl, program, "point_lights", index, "position"),
			color: uniform_index!(gl, program, "point_lights", index, "color"),
			ambient: uniform_index!(gl, program, "point_lights", index, "ambient"),
			diffuse: uniform_index!(gl, program, "point_lights", index, "diffuse"),
			specular: uniform_index!(gl, program, "point_lights", index, "specular"),
			constant: uniform_index!(gl, program, "point_lights", index, "constant"),
			linear: uniform_index!(gl, program, "point_lights", index, "linear"),
			quadratic: uniform_index!(gl, program, "point_lights", index, "quadratic")
		}
	}
}

pub struct SpotLightUniforms {
	pub position: WebGlUniformLocation,
	pub direction: WebGlUniformLocation,
	pub inner_cutoff: WebGlUniformLocation,
	pub outer_cutoff: WebGlUniformLocation,

	pub color: WebGlUniformLocation,
	pub ambient: WebGlUniformLocation,
	pub diffuse: WebGlUniformLocation,
	pub specular: WebGlUniformLocation
}

impl SpotLightUniforms {
	fn new(gl: &GL, program: &WebGlProgram, index: usize) -> Self {
		Self {
			position: uniform_index!(gl, program, "spot_lights", index, "position"),
			direction: uniform_index!(gl, program, "spot_lights", index, "direction"),
			inner_cutoff: uniform_index!(gl, program, "spot_lights", index, "inner_cutoff"),
			outer_cutoff: uniform_index!(gl, program, "spot_lights", index, "outer_cutoff"),
			color: uniform_index!(gl, program, "spot_lights", index, "color"),
			ambient: uniform_index!(gl, program, "spot_lights", index, "ambient"),
			diffuse: uniform_index!(gl, program, "spot_lights", index, "diffuse"),
			specular: uniform_index!(gl, program, "spot_lights", index, "specular"),
		}
	}
}

pub struct Uniforms {
	pub mvp: WebGlUniformLocation,
	pub camera_position: WebGlUniformLocation,
	pub material: MaterialUniforms,
	pub point_dynamic_fluctuation: WebGlUniformLocation,

	pub directional_light: DirectionalLightUniforms,
	pub point_lights: [PointLightUniforms; POINT_LIGHT_LIMIT],
	pub spot_lights: [SpotLightUniforms; SPOT_LIGHT_LIMIT]
}

pub struct BasicShader {
	pub program: WebGlProgram,
	pub attribs: Attribs,
	pub uniforms: Uniforms
}

impl BasicShader {
	pub fn new(gl: &GL) -> Result<Self, String> {
		let program = SELF.link(gl)?;
		let attribs = Attribs {
			position: attrib!(gl, program, "position"),
			color: attrib!(gl, program, "color"),
			normal: attrib!(gl, program, "normal")
		};
		let uniforms = Uniforms {
			mvp: uniform!(gl, &program, "mvp"),
			camera_position: uniform!(gl, &program, "camera_position"),
			material: MaterialUniforms::new(gl, &program),
			point_dynamic_fluctuation: uniform!(gl, &program, "point_dynamic_fluctuation"),
			directional_light: DirectionalLightUniforms::new(gl, &program),
			point_lights: [
				PointLightUniforms::new(gl, &program, 0),
				PointLightUniforms::new(gl, &program, 1),
				PointLightUniforms::new(gl, &program, 2),
				PointLightUniforms::new(gl, &program, 3),
				PointLightUniforms::new(gl, &program, 4),
				PointLightUniforms::new(gl, &program, 5),
				PointLightUniforms::new(gl, &program, 6),
				PointLightUniforms::new(gl, &program, 7),
				PointLightUniforms::new(gl, &program, 8),
				PointLightUniforms::new(gl, &program, 9)
			],
			spot_lights: [
				SpotLightUniforms::new(gl, &program, 0),
				SpotLightUniforms::new(gl, &program, 1),
				SpotLightUniforms::new(gl, &program, 2),
				SpotLightUniforms::new(gl, &program, 3),
				SpotLightUniforms::new(gl, &program, 4),
				SpotLightUniforms::new(gl, &program, 5),
				SpotLightUniforms::new(gl, &program, 6),
				SpotLightUniforms::new(gl, &program, 7),
				SpotLightUniforms::new(gl, &program, 8),
				SpotLightUniforms::new(gl, &program, 9)
			]
		};
		Ok(Self {
			program,
			attribs,
			uniforms
		})
	}
}
