#version 300 es

#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif

layout(location = 0) out vec4 color;

in vec4 fragment_color;
in vec3 fragment_normal;
in vec3 fragment_position;

struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

struct DirectionalLight {
	vec3 direction;

	vec3 color;
	float ambient;
	float diffuse;
	float specular;
};

struct PointLight {
	vec3 position;

	vec3 color;
	float ambient;
	float diffuse;
	float specular;

	float constant;
	float linear;
	float quadratic;
};

struct SpotLight {
	vec3 position;
	vec3 direction;
	float inner_cutoff;
	float outer_cutoff;

	vec3 color;
	float ambient;
	float diffuse;
	float specular;
};

const int POINT_LIGHT_LIMIT = 10;
const int SPOT_LIGHT_LIMIT = 10;

uniform vec3 camera_position;
uniform Material material;
uniform float point_dynamic_fluctuation;
uniform DirectionalLight directional_light;
uniform PointLight point_lights[POINT_LIGHT_LIMIT];
uniform SpotLight spot_lights[SPOT_LIGHT_LIMIT];

vec3 ambient_contribution(vec3 light_color, float ambient_factor) {
	return light_color * ambient_factor * material.ambient;
}

vec3 diffuse_contribution(vec3 light_color, float diffuse_factor, vec3 normal, vec3 light_direction) {
	float diffuse_impact = max(dot(normal, light_direction), 0.0);
	return light_color * diffuse_factor * diffuse_impact * material.diffuse;
}

vec3 specular_contribution(vec3 light_color, float specular_factor, vec3 normal, vec3 light_direction, vec3 view_direction) {
	vec3 halfway_direction = normalize(light_direction + view_direction);
	float specular_impact = pow(max(dot(normal, halfway_direction), 0.0), material.shininess);
	return light_color * specular_impact * specular_impact * material.specular;
}

vec3 light_contributions(vec3 light_direction, vec3 light_color, float ambient_factor, float diffuse_factor, float specular_factor, vec3 normal, vec3 view_direction) {
	vec3 ambient = ambient_contribution(light_color, ambient_factor);
	vec3 diffuse = diffuse_contribution(light_color, diffuse_factor, normal, light_direction);
	vec3 specular = specular_contribution(light_color, specular_factor, normal, light_direction, view_direction);
	return ambient + diffuse + specular;
}

vec3 calculate_directional_light(DirectionalLight light, vec3 normal, vec3 view_direction) {
	vec3 light_direction = normalize(-light.direction);
	return light_contributions(light_direction, light.color, light.ambient, light.diffuse, light.specular, normal, view_direction);
}

vec3 calculate_point_light(PointLight light, vec3 normal, vec3 view_direction) {
	vec3 light_direction = normalize(light.position - fragment_position);

	float distance = length(light.position - fragment_position);
	float inverse_attenuation = light.constant + light.linear * distance + light.quadratic * distance * distance;
	if (inverse_attenuation == 0.0) {
		return vec3(0.0, 0.0, 0.0);
	}
	float attenuation = 1.0 / inverse_attenuation;

	vec3 contributions = light_contributions(light_direction, light.color, light.ambient, light.diffuse, light.specular, normal, view_direction);
	return contributions * attenuation * point_dynamic_fluctuation;
}

vec3 calculate_spot_light(SpotLight light, vec3 normal, vec3 view_direction) {
	vec3 absolute_light_direction = normalize(light.position - fragment_position);
	float theta = dot(absolute_light_direction, normalize(-light.direction));
	float epsilon = light.inner_cutoff - light.outer_cutoff;
	float intensity = clamp((theta - light.outer_cutoff) / epsilon, 0.0, 1.0);

	vec3 ambient = ambient_contribution(light.color, light.ambient);
	vec3 diffuse = diffuse_contribution(light.color, light.diffuse, normal, absolute_light_direction) * intensity;
	vec3 specular = specular_contribution(light.color, light.specular, normal, absolute_light_direction, view_direction) * intensity;

	return ambient + diffuse + specular;
}

void main() {
	vec3 normal = normalize(fragment_normal);
	vec3 view_direction = normalize(camera_position - fragment_position);

	vec3 result = calculate_directional_light(directional_light, normal, view_direction);
	for (int i = 0; i < POINT_LIGHT_LIMIT; ++i) {
		result += calculate_point_light(point_lights[i], normal, view_direction);
	}
	for (int i = 0; i < SPOT_LIGHT_LIMIT; ++i) {
		result += calculate_spot_light(spot_lights[i], normal, view_direction);
	}

	color = fragment_color * vec4(result, 1.0);
}
