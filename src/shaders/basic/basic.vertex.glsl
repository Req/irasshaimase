#version 300 es

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
layout(location = 2) in vec3 normal;

out vec4 fragment_color;
out vec3 fragment_normal;
out vec3 fragment_position;

uniform mat4 mvp;

void main()
{
	fragment_color = color;
	fragment_normal = normal;
	fragment_position = position;
	gl_Position = mvp * vec4(position, 1);
}
