
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Color {
	pub r: u8,
	pub g: u8,
	pub b: u8,
	pub a: u8
}

fn clamp<T>(value: T, min: T, max: T) -> T where T: PartialOrd {
	if value < min {
		min
	} else if value > max {
		max
	} else {
		value
	}
}

fn u8_from_percent(percent: f32) -> u8 {
	assert!((0.0..=1.0).contains(&percent));
	(255.0 * percent) as u8
}

#[allow(unused)]
impl Color {
	pub const fn grey(g: u8) -> Self {
		Self::rgb(g, g, g)
	}

	pub const fn rgb(r: u8, g: u8, b: u8) -> Self {
		Self::rgba(r, g, b, 255)
	}

	pub const fn rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
		Self { r, g, b, a }
	}

	pub const fn is_transparent(&self) -> bool {
		self.a == 0
	}

	pub fn scale(&self, weight: f32) -> Self {
		Self::rgba(
			clamp(self.r as f32 * weight, 0.0, 255.0) as u8,
			clamp(self.g as f32 * weight, 0.0, 255.0) as u8,
			clamp(self.b as f32 * weight, 0.0, 255.0) as u8,
			self.a
		)
	}

	pub fn scale_a(&self, weight: f32) -> Self {
		Self::rgba(
			clamp(self.r as f32 * weight, 0.0, 255.0) as u8,
			clamp(self.g as f32 * weight, 0.0, 255.0) as u8,
			clamp(self.b as f32 * weight, 0.0, 255.0) as u8,
			clamp(self.a as f32 * weight, 0.0, 255.0) as u8
		)
	}

	pub fn as_greyscale(&self) -> Self {
		let rw = self.r as f32 * 0.299;
		let gw = self.g as f32 * 0.587;
		let bw = self.b as f32 * 0.114;
		let grey = u8_from_percent(clamp((rw + gw + bw) / 255.0, 0.0, 1.0));
		Self::rgba(grey, grey, grey, self.a)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn constructors() {
		let mut c = Color::grey(5);
		assert_eq!(c, Color::rgba(5, 5, 5, 255));

		c = Color::rgb(1, 2, 3);
		assert_eq!(c, Color::rgba(1, 2, 3, 255));

		c = Color::rgba(1, 2, 3, 4);
		assert_eq!(c, Color::rgba(1, 2, 3, 4));
	}

	#[test]
	fn scale() {
		let c = Color::rgba(10, 20, 30, 40);
		assert_eq!(c.scale(1.0), c);
		assert_eq!(c.scale(2.0), Color::rgba(20, 40, 60, 40));
		assert_eq!(c.scale(0.5), Color::rgba(5, 10, 15, 40));

		assert_eq!(c.scale_a(1.0), c);
		assert_eq!(c.scale_a(2.0), Color::rgba(20, 40, 60, 80));
		assert_eq!(c.scale_a(0.5), Color::rgba(5, 10, 15, 20));
	}

	#[test]
	fn as_greyscale() {
		let c = Color::rgb(
			(25.0 / 0.299) as u8,
			(25.0 / 0.587) as u8,
			(25.0 / 0.114) as u8,
		);
		// the expectation is 75, but any loss at all causes it to truncate to 74
		assert_eq!(c.as_greyscale(), Color::grey(74));
	}
}
