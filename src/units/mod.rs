
mod color;

pub use color::Color;

pub trait Numeric:
	Copy +
	num::Num +
	PartialOrd
	{}

impl<T> Numeric for T where T:
	Copy +
	num::Num +
	PartialOrd
	{}
