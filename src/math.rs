
use glm::{
	Mat4,
	vec3,
	Vec3,
	vec4
};

#[inline]
pub fn rotation_matrix(rotation: Vec3) -> Mat4 {
	let r = glm::ext::rotate(&num::one(), rotation.z, glm::vec3(0.0, 0.0, 1.0));
	let r = glm::ext::rotate(         &r, rotation.y, glm::vec3(0.0, 1.0, 0.0));
	        glm::ext::rotate(         &r, rotation.x, glm::vec3(1.0, 0.0, 0.0))
}

#[inline]
pub fn rotate_around_with(point: Vec3, origin: Vec3, rotation: &Mat4) -> Vec3 {
	// translate to absolute origin
	let absolute = point - origin;
	// rotate
	let rotated = *rotation * vec4(absolute.x, absolute.y, absolute.z, 0.0);
	// translate back to relative origin
	vec3(rotated.x, rotated.y, rotated.z) + origin
}

#[inline]
pub fn rotate_around(point: Vec3, origin: Vec3, rotation: Vec3) -> Vec3 {
	if rotation == num::zero() {
		return point;
	}
	rotate_around_with(point, origin, &rotation_matrix(rotation))
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::f32::consts::PI;

	macro_rules! assert_vec_eq {
		($result: expr, $expectation: expr) => {{
			assert_approx_eq!($result.x, $expectation.x);
			assert_approx_eq!($result.y, $expectation.y);
			assert_approx_eq!($result.z, $expectation.z);
		}}
	}

	#[test]
	fn rotate_around_absolute_origin() {
		let origin = num::zero();

		let point = vec3(1.0, 2.0, 3.0);
		let rotation = vec3(PI / 2.0, 0.0, 0.0);
		let result = rotate_around(point, origin, rotation);
		let expectation = vec3(1.0, -3.0, 2.0);
		assert_vec_eq!(result, expectation);

		let point = vec3(1.0, 2.0, 3.0);
		let rotation = vec3(PI / 2.0, PI, 0.0);
		let result = rotate_around(point, origin, rotation);
		let expectation = vec3(-1.0, -3.0, -2.0);
		assert_vec_eq!(result, expectation);

		let point = vec3(1.0, 2.0, 3.0);
		let rotation = vec3(PI / 2.0, PI, PI * 1.5);
		let result = rotate_around(point, origin, rotation);
		let expectation = vec3(-3.0, 1.0, -2.0);
		assert_vec_eq!(result, expectation);
	}

	#[test]
	fn rotate_around_relative_origin() {
		// this is the same as the cumulative step of rotate_around_absolute_origin but shifted by (1, 1, 1)
		let origin = num::one();

		let point = vec3(2.0, 3.0, 4.0);
		let rotation = vec3(PI / 2.0, PI, PI * 1.5);
		let result = rotate_around(point, origin, rotation);
		let expectation = vec3(-2.0, 2.0, -1.0);
		assert_vec_eq!(result, expectation);
	}
}
