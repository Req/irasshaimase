
#![warn(rust_2018_idioms)]

#[macro_use]
extern crate const_zero;
#[macro_use]
extern crate memoffset;

#[allow(unused)]
macro_rules! assert_approx_eq {
	($result: expr, $expectation: expr) => {{
		const APPROX_EQ_RADIUS: f32 = 0.00001;
		if !($expectation - APPROX_EQ_RADIUS < $result && $result < $expectation + APPROX_EQ_RADIUS) {
			panic!("assertion failed: {:?} and {:?} are not approximately equal", $result, $expectation);
		}
	}}
}

#[macro_use]
mod wrappers;
mod camera;
mod engine;
mod geometry;
mod light;
mod material;
mod math;
mod scene;
mod shaders;
mod units;

use wasm_bindgen::prelude::wasm_bindgen;
use web_sys::HtmlCanvasElement;
use crate::{
	engine::Context,
	wrappers::by_id
};

#[wasm_bindgen(start)]
pub fn start() {
	let canvas = by_id::<HtmlCanvasElement>("main-canvas").unwrap();
	match Context::new(&canvas) {
		Ok(context) => context.run_loop(canvas),
		Err(s) => log!("When creating context:\n{}", s)
	}
}

fn main() {}
