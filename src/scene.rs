
use std::f32::consts::PI;
use glm::vec3;
use crate::{
	geometry::{
		Clock,
		Geometry,
		Lamp,
		Rectangle,
		RectangularPrism,
		Water
	},
	light::DirectionalLight,
	material::MaterialId,
	units::Color
};

fn horizontal_plane(location: (f32, f32, f32), width: f32, length: f32, color: Color, material: MaterialId) -> Rectangle {
	Rectangle::new(
		vec3(width, length, 0.0),
		vec3(location.0, location.1, location.2),
		vec3(-PI / 2.0, 0.0, 0.0),
		color,
		material
	)
}

fn z_beam(location: (f32, f32, f32), size: f32, length: f32, color: Color, material: MaterialId) -> RectangularPrism {
	RectangularPrism::new(
		vec3(size, size, length),
		vec3(location.0, location.1, location.2),
		num::zero(),
		num::zero(),
		color,
		material
	)
}

fn y_beam(location: (f32, f32, f32), size: f32, length: f32, color: Color, material: MaterialId) -> RectangularPrism {
	RectangularPrism::new(
		vec3(size, length, size),
		vec3(location.0, location.1, location.2),
		num::zero(),
		num::zero(),
		color,
		material
	)
}

pub struct Scene {}

impl Scene {
	pub const GLOBAL_LIGHT: DirectionalLight = DirectionalLight {
		direction: [0.0, -1.0, 0.0],
		color: [248.0 / 255.0, 226.0 / 255.0, 205.0 / 255.0],
		ambient: 0.5,
		diffuse: 0.5,
		specular: 1.0
	};

	pub fn build() -> Vec<Box<dyn Geometry>> {
		let mut scene: Vec<Box<dyn Geometry>> = Vec::new();

		let concrete = Color::grey(200);
		let street_length = 100.0;
		let street_width = 4.0;
		let street_border_width = street_width / 10.0;
		let street = horizontal_plane((0.0, 0.0, 0.0), street_width, street_length, concrete, MaterialId::Concrete);
		let street_left_border = z_beam((-street_width / 2.0, -0.13, 0.0), street_border_width, street_length, concrete, MaterialId::Concrete);
		let street_right_border = z_beam((street_width / 2.0, -0.13, 0.0), street_border_width, street_length, concrete, MaterialId::Concrete);
		scene.push(Box::new(street));
		scene.push(Box::new(street_left_border));
		scene.push(Box::new(street_right_border));

		let foundation = Color::grey(180);
		let left_foundation_front = -street_width / 1.5;
		let right_foundation_front = street_width / 1.5;
		let left_foundation = RectangularPrism::new(
			vec3(street_length, street_length, street_length),
			vec3(left_foundation_front, 0.0, 0.0),
			vec3(1.0, 1.0, 0.0),
			num::zero(),
			foundation,
			MaterialId::Concrete
		);
		let right_foundation = RectangularPrism::new(
			vec3(street_length, street_length, street_length),
			vec3(right_foundation_front, 0.0, 0.0),
			vec3(-1.0, 1.0, 0.0),
			num::zero(),
			foundation,
			MaterialId::Concrete
		);
		let water_foundation = Water::new(
			vec3(6.0, 0.0, street_length),
			vec3(0.0, -1.0, 0.0)
		);
		scene.push(Box::new(left_foundation));
		scene.push(Box::new(right_foundation));
		scene.push(Box::new(water_foundation));

		let wall_thickness = 0.05;
		let building_height = 8.0;
		let dark_wood = Color::rgb(20, 20, 30);
		let left_wall = RectangularPrism::new(
			vec3(building_height, building_height, wall_thickness),
			vec3(left_foundation_front, 0.0, 0.5),
			vec3(1.0, -1.0, 0.0),
			num::zero(),
			dark_wood,
			MaterialId::DarkWood
		);
		let right_wall = RectangularPrism::new(
			vec3(building_height, building_height, wall_thickness),
			vec3(left_foundation_front, 0.0, -3.0),
			vec3(1.0, -1.0, 0.0),
			num::zero(),
			dark_wood,
			MaterialId::DarkWood
		);
		scene.push(Box::new(left_wall));
		scene.push(Box::new(right_wall));

		let wall_lamp = Lamp::new(
			1.0,
			vec3(left_foundation_front - 0.5, 3.0, -2.5),
			Color::rgb(231, 83, 67)
		);
		scene.push(Box::new(wall_lamp));

		scene.push(Box::new(Clock::new(vec3(0.0, 30.0, -50.0))));

		scene
	}
}
