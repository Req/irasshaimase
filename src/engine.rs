
use std::{
	cell::RefCell,
	cmp::Ordering,
	f32::consts::PI,
	marker::PhantomData,
	rc::Rc
};
use chrono::{
	Duration,
	offset::{
		Local,
		Utc
	}
};
use glm::{
	Mat4,
	Vec3
};
use js_sys::Uint8Array;
use wasm_bindgen::{
	JsCast,
	prelude::Closure
};
use web_sys::{
	HtmlCanvasElement,
	HtmlElement,
	WebGl2RenderingContext,
	WebGlBuffer,
	WebGlVertexArrayObject
};
use crate::{
	camera::Camera,
	geometry::Geometry,
	light::{
		Light,
		PointLight,
		SpotLight
	},
	material::{
		MATERIAL_COUNT,
		MaterialId
	},
	scene::Scene,
	shaders::basic::{
		BasicShader,
		POINT_LIGHT_LIMIT,
		SPOT_LIGHT_LIMIT
	},
	units::Color,
	wrappers::WebExpect
};

// for easier access of constants
pub type GL = WebGl2RenderingContext;

#[allow(unused)]
#[cfg(debug_assertions)]
fn check_gl_error(gl: &GL, file: &str, line: u32) {
	loop {
		match gl.get_error() {
			GL::NO_ERROR => break,
			GL::INVALID_OPERATION             => log!("GL_INVALID_OPERATION: {}:{}", file, line),
			GL::INVALID_ENUM                  => log!("GL_INVALID_ENUM: {}:{}", file, line),
			GL::INVALID_VALUE                 => log!("GL_INVALID_VALUE: {}:{}", file, line),
			GL::OUT_OF_MEMORY                 => log!("GL_OUT_OF_MEMORY: {}:{}", file, line),
			GL::INVALID_FRAMEBUFFER_OPERATION => log!("INVALID_FRAMEBUFFER_OPERATION: {}:{}", file, line),
			other                             => log!("Unknown GL error ({})", other)
		}
	}
}

#[allow(unused)]
#[cfg(not(debug_assertions))]
fn check_gl_error(_context: &GL, _file: &str, _line: u32) {}

#[allow(unused_macros)]
macro_rules! check_gl_error {
	($gl: expr) => {
		check_gl_error(&$gl, std::file!(), std::line!());
	}
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Vertex {
	pub position: Vec3,
	pub color: Color,
	pub normal: Vec3
}

impl Default for Vertex {
	fn default() -> Self {
		Self {
			position: num::zero(),
			color: Default::default(),
			normal: num::zero()
		}
	}
}

impl Vertex {
	pub fn from(position: Vec3, color: Color, normal: Vec3) -> Self {
		Self {
			position,
			color,
			normal
		}
	}
}

#[derive(Debug, Default)]
struct ObjectGroup {
	objects: Vec<Box<dyn Geometry>>,
	start_index: usize,
	span: usize
}

impl ObjectGroup {
	fn init_buffer(&mut self, vertices: &mut Vec<Vertex>) {
		self.start_index = vertices.len();
		let mut index = self.start_index;
		for object in &self.objects {
			vertices.resize(index + object.vertex_count(), Default::default());
			object.update(&mut vertices[index..], TimeDelta::zero());
			index = vertices.len();
		}
		self.span = vertices.len() - self.start_index;
	}

	fn rebuffer(&self, vertices: &mut [Vertex], time: &TimeDelta) {
		assert_eq!(vertices.len(), self.span);
		let mut start = 0;
		for object in &self.objects {
			let end = start + object.vertex_count();
			object.update(&mut vertices[start..end], time);
			start = end;
		}
	}
}

#[derive(Debug, Default)]
struct StaticDynamicGroup {
	static_group: ObjectGroup,
	dynamic_group: ObjectGroup
}

// not semantically correct, but satisfies the impl Clone requirement
impl Clone for StaticDynamicGroup {
	fn clone(&self) -> Self {
		Default::default()
	}
}

/// Produces a time-based fluctuating value.
/// This value is used to make point lights "pulse".
struct DynamicFluctuator {
	elapsed: i64
}

impl DynamicFluctuator {
	fn new() -> Self {
		Self {
			elapsed: 0
		}
	}

	fn tick(&mut self, time: &TimeDelta) -> f32 {
		const PERIOD: i64 = 850;
		const AMPLITUDE: f32 = 0.05;

		self.elapsed = (self.elapsed + time.delta) % PERIOD;
		let progress = self.elapsed as f32 / PERIOD as f32 * PI * 2.0;
		let fluctuation = progress.sin() * AMPLITUDE;
		1.0 + fluctuation
	}
}

pub struct Context {
	context: GL,
	shader: BasicShader,
	vao: WebGlVertexArrayObject,
	camera: Camera,
	fluctuator: DynamicFluctuator,

	vertex_buffer: WebGlBuffer,
	vertices: Vec<Vertex>,
	groups: [StaticDynamicGroup; MATERIAL_COUNT],
	point_lights: [PointLight; POINT_LIGHT_LIMIT],
	spot_lights: [SpotLight; SPOT_LIGHT_LIMIT]
}

fn request_animation_frame(f: &Closure<dyn FnMut()>) {
	web_sys::window().unwrap().request_animation_frame(f.as_ref().unchecked_ref()).unwrap();
}

fn is_pow2(a: usize) -> bool {
	(a & (a - 1)) == 0
}

fn view_bytes<T>(xs: &[T]) -> (Uint8Array, PhantomData<&[T]>) {
	assert!(is_pow2(std::mem::align_of::<Vertex>()));

	let size = std::mem::size_of::<T>();
	unsafe {
		/*
		 * There's no nice way to pass a packed Vertex, so it has to be
		 * presented to the API as a byte buffer. view() can do that with a
		 * transmute, but it queries the slice for its length, which yields
		 * an incorrect result (count of T instead of count of sizeof(T)).
		 * view_mut_raw() allows you to specify the size, but expects a mut
		 * pointer. The source for the function doesn't appear to mutate the
		 * contents and this function returns a non-mut view, and the mut&
		 * points to something the compiler isn't allowed to trample, so it
		 * doesn't appear that I'm violating anything serious by doing this.
		 */
		#[allow(mutable_transmutes)]
		let byte_slice: &mut [u8] = std::mem::transmute(xs);
		let view = Uint8Array::view_mut_raw(byte_slice.as_mut_ptr(), size * xs.len());
		(view, PhantomData)
	}
}

trait IsFloatBuffer {}
impl IsFloatBuffer for Mat4 {}
impl IsFloatBuffer for Vec3 {}

fn view_floats<T>(m: &T) -> &[f32] where T: IsFloatBuffer {
	unsafe {
		let ptr: *const T = m;
		let ptr: *const f32 = std::mem::transmute(ptr);
		let count = std::mem::size_of::<T>() / std::mem::size_of::<f32>();
		std::slice::from_raw_parts(ptr, count)
	}
}

// repr(C) is to ensure UB doesn't happen in ZERO
#[repr(C)]
pub struct TimeDelta {
	pub delta: i64,
	pub now_absolute: i64,
	pub now_relative: Duration
}

impl TimeDelta {
	const ZERO: Self = Self {
		delta: 0,
		now_absolute: 0,
		// duration is 2 integers internally. zeroing this is fine
		now_relative: unsafe { const_zero!(Duration) }
	};

	pub fn new() -> Self {
		let local_now = Local::now();
		let last_midnight = local_now.date().and_hms(0, 0, 0);

		Self {
			delta: 0,
			now_absolute: Utc::now().timestamp_millis(),
			now_relative: local_now.signed_duration_since(last_midnight)
		}
	}

	pub fn from(other: &TimeDelta) -> Self {
		let mut new = Self::new();
		new.delta = new.now_absolute - other.now_absolute;
		new
	}

	pub const fn zero() -> &'static Self {
		&Self::ZERO
	}
}

fn match_light_limit<T>(lights: &mut Vec<T>, limit: usize) where T: Clone + Default {
	match lights.len().cmp(&limit) {
		Ordering::Greater => panic!("Too many light sources"),
		Ordering::Less => lights.resize(limit, Default::default()),
		Ordering::Equal => {}
	}
}

impl Context {
	pub fn new(canvas: &HtmlCanvasElement) -> Result<Self, String> {
		let gl = match canvas.get_context("webgl2") {
			Ok(Some(gl)) => gl.dyn_into::<GL>().unwrap(),
			_ => return Err(String::from("Unable to get webgl2 context"))
		};
		gl.enable(GL::DEPTH_TEST);

		let shader = BasicShader::new(&gl).expect_log("Cannot create shader");
		let vao = gl.create_vertex_array().expect_log("Cannot create VAO");
		gl.bind_vertex_array(Some(&vao));
		let vertex_buffer = gl.create_buffer().expect_log("Cannot create vertex buffer");
		gl.bind_buffer(GL::ARRAY_BUFFER, Some(&vertex_buffer));
		gl.enable_vertex_attrib_array(shader.attribs.position);
		gl.enable_vertex_attrib_array(shader.attribs.color);
		gl.enable_vertex_attrib_array(shader.attribs.normal);
		let stride: i32 = std::mem::size_of::<Vertex>().try_into().unwrap();
		gl.vertex_attrib_pointer_with_i32(shader.attribs.position, 3,         GL::FLOAT, false, stride, offset_of!(Vertex, position).try_into().unwrap());
		gl.vertex_attrib_pointer_with_i32(shader.attribs.   color, 4, GL::UNSIGNED_BYTE,  true, stride, offset_of!(Vertex,    color).try_into().unwrap());
		gl.vertex_attrib_pointer_with_i32(shader.attribs.  normal, 3,         GL::FLOAT, false, stride, offset_of!(Vertex,   normal).try_into().unwrap());

		let groups = vec![StaticDynamicGroup::default(); MATERIAL_COUNT];
		let mut groups: [StaticDynamicGroup; MATERIAL_COUNT] = groups.try_into().unwrap();
		let mut point_lights = Vec::with_capacity(10);
		let mut spot_lights = Vec::with_capacity(10);
		for object in Scene::build() {
			match object.attached_lighting() {
				Some(Light::Point(point)) => point_lights.push(point.clone()),
				Some(Light::Spot(spot)) => spot_lights.push(spot.clone()),
				None => {}
			}

			let material = object.material().index();
			if object.is_dynamic() {
				groups[material].dynamic_group.objects.push(object);
			} else {
				groups[material].static_group.objects.push(object);
			}
		}
		match_light_limit(&mut point_lights, POINT_LIGHT_LIMIT);
		match_light_limit(&mut spot_lights, SPOT_LIGHT_LIMIT);
		let mut vertices = Vec::new();
		for group in &mut groups {
			group.static_group.init_buffer(&mut vertices);
			group.dynamic_group.init_buffer(&mut vertices);
		}
		let (view, _) = view_bytes(&vertices);
		gl.buffer_data_with_array_buffer_view(GL::ARRAY_BUFFER, &view, GL::DYNAMIC_DRAW); check_gl_error!(gl);

		Ok(Self {
			vao,
			vertex_buffer,
			shader,
			context: gl,
			camera: Camera::new(),
			vertices,
			groups,
			point_lights: point_lights.try_into().unwrap(),
			spot_lights: spot_lights.try_into().unwrap(),
			fluctuator: DynamicFluctuator::new()
		})
	}

	pub fn run_loop(mut self, canvas: HtmlCanvasElement) {
		let rc = Rc::new(RefCell::new(None));
		let renderer = rc.clone();
		let mut time = TimeDelta::new();
		*renderer.borrow_mut() = Some(Closure::wrap(Box::new(move || {
			time = TimeDelta::from(&time);

			let width = canvas.client_width() as u32;
			let height = canvas.client_height() as u32;
			canvas.set_width(width);
			canvas.set_height(height);
			self.camera.viewport(width, height);

			let label = crate::wrappers::by_id::<HtmlElement>("fps-label").unwrap();
			label.set_inner_text(&format!("frame time: {}ms", time.delta));

			self.render(width, height, &time);
			request_animation_frame(rc.borrow().as_ref().unwrap());
		}) as Box<dyn FnMut()>));
		request_animation_frame(renderer.borrow().as_ref().unwrap());
	}

	fn render(&mut self, width: u32, height: u32, time: &TimeDelta) {
		let gl = &self.context;
		gl.viewport(0, 0, width as i32, height as i32);

		gl.use_program(Some(&self.shader.program)); check_gl_error!(gl);
		gl.bind_vertex_array(Some(&self.vao)); check_gl_error!(gl);
		gl.bind_buffer(GL::ARRAY_BUFFER, Some(&self.vertex_buffer));

		let mvp = self.camera.perspective() * self.camera.view();
		gl.uniform_matrix4fv_with_f32_array(Some(&self.shader.uniforms.mvp), false, view_floats(&mvp));

		let camera_position = self.camera.position();
		let camera_position = [camera_position.x, camera_position.y, camera_position.z];
		gl.uniform3fv_with_f32_array(Some(&self.shader.uniforms.camera_position), &camera_position);

		gl.uniform1f(Some(&self.shader.uniforms.point_dynamic_fluctuation), self.fluctuator.tick(time));

		Scene::GLOBAL_LIGHT.uniform(gl, &self.shader);
		for (index, point) in self.point_lights.iter().enumerate() {
			point.uniform(gl, &self.shader, index);
		}
		for (index, spot) in self.spot_lights.iter().enumerate() {
			spot.uniform(gl, &self.shader, index);
		}

		for (id, group) in self.groups.iter().enumerate() {
			let material = MaterialId::from_index(id).material();
			gl.uniform3fv_with_f32_array(Some(&self.shader.uniforms.material.ambient), view_floats(&material.ambient));
			gl.uniform3fv_with_f32_array(Some(&self.shader.uniforms.material.diffuse), view_floats(&material.diffuse));
			gl.uniform3fv_with_f32_array(Some(&self.shader.uniforms.material.specular), view_floats(&material.specular));
			gl.uniform1f(Some(&self.shader.uniforms.material.shininess), material.shininess);

			const VERTEX_SIZE: i32 = std::mem::size_of::<Vertex>() as i32;

			let dynamic = &group.dynamic_group;
			let slice = &mut self.vertices[dynamic.start_index..dynamic.start_index + dynamic.span];
			dynamic.rebuffer(slice, time);
			let (view, _) = view_bytes(slice);
			gl.buffer_sub_data_with_i32_and_array_buffer_view(GL::ARRAY_BUFFER, dynamic.start_index as i32 * VERTEX_SIZE, &view); check_gl_error!(gl);

			let statik = &group.static_group;
			let offset = statik.start_index as i32;
			let count = (statik.span + dynamic.span) as i32;
			gl.draw_arrays(GL::TRIANGLES, offset, count); check_gl_error!(gl);
		}
	}
}
