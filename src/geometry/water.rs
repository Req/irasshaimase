
use std::f32::consts::PI;
use glm::{
	vec3,
	Vec3
};
use crate::{
	engine::{
		TimeDelta,
		Vertex
	},
	geometry::{
		Geometry,
		Rectangle
	},
	material::MaterialId,
	units::Color
};

#[derive(Debug)]
pub struct Water {
	size: Vec3,
	center: Vec3
}

impl Water {
	const WAVE_FREQUENCY: usize = 50;
	const FLOW_DURATION: i64 = 1500;
	const CREST_SIZE: f32 = 0.1;
	const WATER_COLOR: Color = Color::rgb(40, 100, 200);
	const CREST_COLOR: Color = Color::rgb(30, 90, 210);

	pub fn new(size: Vec3, center: Vec3) -> Self {
		Self {
			size,
			center
		}
	}
}

impl Geometry for Water {
	fn vertex_count(&self) -> usize {
		// WAVE_FREQUENCY waves on each side, plus the main body
		Rectangle::VERTEX_COUNT * (Self::WAVE_FREQUENCY * 2 + 1)
	}

	fn material(&self) -> MaterialId {
		MaterialId::Water
	}

	fn is_dynamic(&self) -> bool {
		true
	}

	fn update(&self, vertices: &mut [Vertex], time: &TimeDelta) {
		let body = Rectangle::new(
			vec3(self.size.x, self.size.z, 0.0),
			self.center,
			vec3(-PI / 2.0, 0.0, 0.0),
			Self::WATER_COLOR,
			self.material()
		);
		body.update(&mut vertices[..Rectangle::VERTEX_COUNT], time);

		let flow_offset = (time.now_absolute % Self::FLOW_DURATION) as f32 / Self::FLOW_DURATION as f32;
		let spacing = self.size.z / Self::WAVE_FREQUENCY as f32;
		for i in 0..Self::WAVE_FREQUENCY {
			let traveled = i as f32 * spacing + flow_offset * spacing;
			// going from positive to negative, so subtract the offset
			let z = self.size.z / 2.0 - traveled;

			let left = Rectangle::new(
				vec3(self.size.x / 2.0, Self::CREST_SIZE, 0.0),
				vec3(self.center.x - self.size.x / 2.0, self.center.y + 0.01, z),
				vec3(-PI / 2.0, PI / 6.0, 0.0),
				Self::CREST_COLOR,
				self.material()
			);
			let right = Rectangle::new(
				vec3(self.size.x / 2.0, Self::CREST_SIZE, 0.0),
				vec3(self.center.x + self.size.x / 2.0, self.center.y + 0.01, z),
				vec3(-PI / 2.0, -PI / 6.0, 0.0),
				Self::CREST_COLOR,
				self.material()
			);

			let start = Rectangle::VERTEX_COUNT + i * Rectangle::VERTEX_COUNT * 2;
			let mid = start + Rectangle::VERTEX_COUNT;
			let end = mid + Rectangle::VERTEX_COUNT;
			left.update(&mut vertices[start..mid], time);
			right.update(&mut vertices[mid..end], time);
		}
	}
}
