
use glm::{
	vec3,
	Vec3
};
use crate::{
	engine::{
		TimeDelta,
		Vertex
	},
	geometry::{
		Geometry,
		RectangularPrism
	},
	light::{
		Light,
		PointLight
	},
	material::MaterialId,
	units::Color
};

#[derive(Debug)]
pub struct Lamp {
	scale: f32,
	position: Vec3,
	body_color: Color,
	light: Light
}

impl Lamp {
	pub fn new(scale: f32, position: Vec3, color: Color) -> Self {
		Self {
			scale,
			position,
			body_color: color,
			light: Light::Point(PointLight {
				position: [position.x, position.y, position.z],
				color: [color.r as f32 / 255.0, color.g as f32 / 255.0, color.b as f32 / 255.0],
				ambient: 0.5,
				diffuse: 1.5,
				specular: 1.0,
				constant: 1.0,
				linear: 0.09,
				quadratic: 0.032
			})
		}
	}
}

const BODY_MATERIAL: MaterialId = MaterialId::Paper;
const FRAME_MATERIAL: MaterialId = MaterialId::LightWood;

impl Geometry for Lamp {
	fn vertex_count(&self) -> usize {
		// 1 body + 2 caps + 4 borders
		RectangularPrism::VERTEX_COUNT * 7
	}

	fn material(&self) -> MaterialId {
		BODY_MATERIAL
	}

	fn attached_lighting(&self) -> Option<&Light> {
		Some(&self.light)
	}

	fn update(&self, vertices: &mut [Vertex], time: &TimeDelta) {
		let default_body_size = vec3(0.2, 0.5, 0.2);
		let default_cap_size = vec3(0.25, 0.075, 0.25);
		let default_border_size = vec3(
			(default_cap_size.x - default_body_size.x) / 2.0,
			default_body_size.y,
			(default_cap_size.z - default_body_size.z) / 2.0
		);

		let body_size = default_body_size * self.scale;
		let cap_size = default_cap_size * self.scale;
		let border_size = default_border_size * self.scale;

		const FRAME_COLOR: Color = Color::grey(0);

		let parts = [
			// body
			RectangularPrism::new(  body_size, self.position                                                                  ,            num::zero(), num::zero(), self.body_color,  BODY_MATERIAL),
			// top cap
			RectangularPrism::new(   cap_size, self.position + vec3(               0.0, body_size.y / 2.0, 0.0               ), vec3( 0.0, -1.0,  0.0), num::zero(),     FRAME_COLOR, FRAME_MATERIAL),
			// bottom cap
			RectangularPrism::new(   cap_size, self.position - vec3(               0.0, body_size.y / 2.0, 0.0               ), vec3( 0.0,  1.0,  0.0), num::zero(),     FRAME_COLOR, FRAME_MATERIAL),
			// front left border
			RectangularPrism::new(border_size, self.position + vec3(-body_size.x / 2.0,               0.0,  body_size.z / 2.0), vec3( 1.0,  0.0, -1.0), num::zero(),     FRAME_COLOR, FRAME_MATERIAL),
			// front right border
			RectangularPrism::new(border_size, self.position + vec3( body_size.x / 2.0,               0.0,  body_size.z / 2.0), vec3(-1.0,  0.0, -1.0), num::zero(),     FRAME_COLOR, FRAME_MATERIAL),
			// back left border
			RectangularPrism::new(border_size, self.position + vec3(-body_size.x / 2.0,               0.0, -body_size.z / 2.0), vec3( 1.0,  0.0,  1.0), num::zero(),     FRAME_COLOR, FRAME_MATERIAL),
			// back right border
			RectangularPrism::new(border_size, self.position + vec3( body_size.x / 2.0,               0.0, -body_size.z / 2.0), vec3(-1.0,  0.0,  1.0), num::zero(),     FRAME_COLOR, FRAME_MATERIAL)
		];

		let mut index = 0;
		for part in parts.into_iter() {
			let slice = &mut vertices[index..index + RectangularPrism::VERTEX_COUNT];
			part.update(slice, time);
			index += RectangularPrism::VERTEX_COUNT;
		}
	}
}
