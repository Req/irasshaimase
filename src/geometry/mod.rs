
mod clock;
mod lamp;
mod rectangle;
mod rectangular_prism;
mod water;

pub use clock::*;
pub use lamp::*;
pub use rectangle::*;
pub use rectangular_prism::*;
pub use water::*;

use std::fmt::{
	Debug,
	Error,
	Formatter
};
use crate::{
	engine::{
		TimeDelta,
		Vertex
	},
	light::Light,
	material::MaterialId
};

pub trait Geometry {
	fn vertex_count(&self) -> usize;

	fn material(&self) -> MaterialId;

	fn attached_lighting(&self) -> Option<&Light> {
		None
	}

	/// The provided slice will have length equal to vertex_count.
	fn update(&self, _vertices: &mut [Vertex], _time: &TimeDelta) {}

	fn is_dynamic(&self) -> bool {
		false
	}
}

// this is mostly just to satisfy an impl Debug requirement
impl Debug for dyn Geometry {
	fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
		write!(f, "Object with {} vertices", self.vertex_count())
	}
}
