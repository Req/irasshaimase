
/*
 * This is derived from egui, which originally derived it from another source.
 *
 * Vulpi Xeshe — Today at 2:34 PM
 * What about the ominous demon portal to the geometry dimension in the back
 */

use std::f32::consts::PI;
use glm::{
	vec3,
	Vec3
};
use crate::{
	engine::{
		TimeDelta,
		Vertex
	},
	geometry::{
		Geometry,
		Rectangle
	},
	light::{
		Light,
		SpotLight
	},
	material::MaterialId,
	math,
	units::Color
};

const BASE_WIDTH: f32 = 0.2;
const BASE_LENGTH: f32 = 5.0;
const BASE_LUMINANCE: f32 = 1.0;
const WIDTH_FACTOR: f32 = 0.9;
const LENGTH_FACTOR: f32 = 0.8;
const LUMINANCE_FACTOR: f32 = 0.8;
const DEPTH: i32 = if cfg!(debug_assertions) {
	5
} else {
	9
};

#[derive(Debug)]
pub struct Clock {
	origin: Vec3,
	line_width: f32,
	line_length: f32,
	luminance: f32,
	depth: i32,
	light: Option<Light>
}

const MATERIAL: MaterialId = MaterialId::LightWood;

/// This should give the exact maximum radius of a clock of depth `d`. Rationale:
///
/// The initial arms have a length of `BASE_LENGTH`.
/// Secondary arms have have a length equal to `BASE_LENGTH * (LENGTH_FACTOR) ^ depth`.
/// The maximum radius of the clock is the cumulative sum of these increasingly-small arms.
/// Example radii for a given depth:
/// ```
/// f(0) = BASE_LENGTH
/// f(1) = BASE_LENGTH + BASE_LENGTH * LENGTH_FACTOR
/// f(2) = BASE_LENGTH + BASE_LENGTH * LENGTH_FACTOR + BASE_LENGTH * LENGTH_FACTOR ^ 2
/// ```
/// And so on. This is a summation of i in [0, d] of `(4 / 5) ^ i * 5`, which itself
/// is a geometric series with `r = (4 / 5)` and `n = d + 1`. The closed form of the
/// maximum radius of the clock is therefore:
/// ```
/// BASE_LENGTH * (1 - LENGTH_FACTOR ^ (d + 1)) / (1 - LENGTH_FACTOR)
/// ```
fn clock_radius(depth: i32) -> f32 {
	BASE_LENGTH * (1.0 - LENGTH_FACTOR.powi(depth + 1)) / (1.0 - LENGTH_FACTOR)
}

impl Clock {
	/// The equation for the number fractal lines given depth d is f(d) = f(d - 1) + 2^(d + 1).
	/// Partial series: 3, 7, 15, 31, ...
	/// The closed form of this is f(d) = 2^(d + 2) - 1.
	const VERTEX_COUNT: usize = (2usize.pow(DEPTH as u32 + 2) - 1) * Rectangle::VERTEX_COUNT;

	pub fn new(origin: Vec3) -> Self {
		/*
		 * The spotlight cutoff angle is 45 degrees, and the spotlight is positioned
		 * offset from the center of the clock, so we have the following triangle:
		 *         /|C
		 *        / |   (BC has a length equal to the maximum radius of the clock)
		 *      A/__|B
		 * Where A is the spotlight's position, B is the center of the clock, and C is
		 * the top of the clock. This is obviously a right triangle. Because the cutoff
		 * (angle A) is 45 degrees, C must also be 45 degrees. Thus it's an equilateral
		 * triangle with AB = BC. Therefore, the spotlight should be positioned
		 * clock_radius units in front.
		 */
		Self {
			origin,
			line_width: BASE_WIDTH,
			line_length: BASE_LENGTH,
			luminance: BASE_LUMINANCE,
			depth: DEPTH,
			light: Some(Light::Spot(SpotLight {
				position: [origin.x, origin.y, origin.z + clock_radius(DEPTH)],
				direction: [0.0, 0.0, -1.0],
				inner_cutoff: (PI / 4.0).cos(),
				outer_cutoff: (PI / 4.0).cos(),
				color: [1.0, 1.0, 1.0],
				ambient: 0.0,
				diffuse: 0.5,
				specular: 0.0
			}))
		}
	}

	/// `arm_percent is the percent the arm has spun around the clock,
	/// starting at the top, and not including the base angle.
	fn buffer_arm(&self, angle: f32, recursive_angles: Option<(f32, f32)>, vertices: &mut [Vertex]) -> usize {
		// the arm is a tall and thin rectangle that gets rotated into the correct orientation
		let rotation = vec3(0.0, 0.0, angle);
		let arm_center = vec3(self.origin.x, self.origin.y + self.line_length / 2.0, self.origin.z);
		let arm_center = math::rotate_around(arm_center, self.origin, rotation);

		let color = Color::rgb(200, 50, 100).scale_a(
			if self.depth != DEPTH {
				self.luminance * 0.7
			} else {
				self.luminance
			}
		);

		let arm = Rectangle::new(
			vec3(self.line_width, self.line_length, 0.0),
			arm_center,
			rotation,
			color,
			MATERIAL
		);
		arm.update(&mut vertices[..arm.vertex_count()], TimeDelta::zero());

		let mut total = Rectangle::VERTEX_COUNT;
		if self.depth == 0 || recursive_angles.is_none() {
			return total;
		}

		let arm_tip = self.origin + (arm_center - self.origin) * 2.0;
		let clock = Clock {
			origin: arm_tip,
			line_width: self.line_width * WIDTH_FACTOR,
			line_length: self.line_length * LENGTH_FACTOR,
			luminance: self.luminance * LUMINANCE_FACTOR,
			depth: self.depth - 1,
			light: None
		};
		let (a, b) = recursive_angles.unwrap();
		total += clock.buffer_arm(angle - a, Some((a, b)), &mut vertices[total..]);
		total += clock.buffer_arm(angle - b, Some((a, b)), &mut vertices[total..]);
		total
	}
}

impl Geometry for Clock {
	fn vertex_count(&self) -> usize {
		Self::VERTEX_COUNT
	}

	fn material(&self) -> MaterialId {
		MATERIAL
	}

	fn attached_lighting(&self) -> Option<&Light> {
		self.light.as_ref()
	}

	fn is_dynamic(&self) -> bool {
		true
	}

	fn update(&self, vertices: &mut [Vertex], time: &TimeDelta) {
		assert_eq!(vertices.len(), Self::VERTEX_COUNT);

		/*
		 * There's no millisecond arm, but it needs to be taken into
		 * consideration to get a smooth animation between seconds.
		 */
		let hours = time.now_relative.num_hours() % 12;
		let minutes = time.now_relative.num_minutes() % 60;
		let seconds = time.now_relative.num_seconds() % 60 % 60;
		let milliseconds = time.now_relative.num_milliseconds() % 1000;

		let millisecond_percent = milliseconds as f32 / 1000.0;
		let second_percent = (seconds as f32 + millisecond_percent) / 60.0;
		let minute_percent = (minutes as f32 + second_percent) / 60.0;
		let hour_percent = (hours as f32 + minute_percent) / 12.0;

		let hour_angle = -2.0 * PI * hour_percent;
		let minute_angle = -2.0 * PI * minute_percent;
		let second_angle = -2.0 * PI * second_percent;
		let hour_minute_angle = hour_angle - minute_angle;
		let hour_second_angle = hour_angle - second_angle;

		let minutes_seconds = Some((hour_minute_angle + PI, hour_second_angle + PI));
		let mut used = self.buffer_arm(hour_angle, None, vertices);
		used += self.buffer_arm(minute_angle, minutes_seconds, &mut vertices[used..]);
		used += self.buffer_arm(second_angle, minutes_seconds, &mut vertices[used..]);
		assert_eq!(used, vertices.len());
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn spotlight_radius() {
		assert_approx_eq!(clock_radius(0), BASE_LENGTH);
		assert_approx_eq!(clock_radius(1), BASE_LENGTH + BASE_LENGTH * LENGTH_FACTOR);
		assert_approx_eq!(clock_radius(2), BASE_LENGTH + BASE_LENGTH * LENGTH_FACTOR + BASE_LENGTH * LENGTH_FACTOR * LENGTH_FACTOR);
	}
}
