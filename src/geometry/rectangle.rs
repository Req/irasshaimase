
use glm::{
	vec3,
	Vec3
};
use crate::{
	engine::{
		TimeDelta,
		Vertex
	},
	geometry::Geometry,
	material::MaterialId,
	math,
	units::Color
};

#[derive(Debug)]
pub struct Rectangle {
	// 3rd dimension is ignored
	size: Vec3,
	center: Vec3,
	rotation: Vec3,
	color: Color,
	material: MaterialId
}

impl Rectangle {
	// 3 indices per triangle * 2 triangles
	pub const VERTEX_COUNT: usize = 3 * 2;

	pub const fn new(size: Vec3, center: Vec3, rotation: Vec3, color: Color, material: MaterialId) -> Self {
		Self {
			size,
			center,
			rotation,
			color,
			material
		}
	}
}

impl Geometry for Rectangle {
	fn vertex_count(&self) -> usize {
		Self::VERTEX_COUNT
	}

	fn material(&self) -> MaterialId {
		self.material
	}

	fn update(&self, vertices: &mut [Vertex], _time: &TimeDelta) {
		assert_eq!(vertices.len(), self.vertex_count());
		let diagonal = self.size / 2.0;
		/*
		 * `rotate_around` calculates the rotation matrix every call.
		 * "Surely it's better performance to calculate it once and
		 * just supply it to the function", you might say, but you
		 * would be wrong. perf hath spoken and the seemingly-inefficient
		 * code runs faster than the efficient code.
		 */
		let     top_left = math::rotate_around(vec3(self.center.x - diagonal.x, self.center.y + diagonal.y, self.center.z), self.center, self.rotation);
		let    top_right = math::rotate_around(vec3(self.center.x + diagonal.x, self.center.y + diagonal.y, self.center.z), self.center, self.rotation);
		let  bottom_left = math::rotate_around(vec3(self.center.x - diagonal.x, self.center.y - diagonal.y, self.center.z), self.center, self.rotation);
		let bottom_right = math::rotate_around(vec3(self.center.x + diagonal.x, self.center.y - diagonal.y, self.center.z), self.center, self.rotation);
		let normal = math::rotate_around(vec3(0.0, 0.0, 1.0), num::zero(), self.rotation);
		vertices.copy_from_slice(&[
			// top left triangle
			Vertex::from(   top_right, self.color, normal),
			Vertex::from(    top_left, self.color, normal),
			Vertex::from( bottom_left, self.color, normal),
			// bottom right triangle
			Vertex::from(   top_right, self.color, normal),
			Vertex::from( bottom_left, self.color, normal),
			Vertex::from(bottom_right, self.color, normal)
		]);
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	fn vertex_buffer(count: usize) -> Vec<Vertex> {
		vec![Default::default(); count]
	}

	fn positions_only(vs: Vec<Vertex>) -> Vec<Vec3> {
		vs.into_iter().map(|v| v.position).collect()
	}

	#[test]
	fn untransformed() {
		let r = Rectangle::new(
			num::one(),
			vec3(1.0, 2.0, 3.0),
			num::zero(),
			Color::grey(255),
			MaterialId::Concrete
		);
		let mut v = vertex_buffer(r.vertex_count());
		r.update(&mut v, TimeDelta::zero());
		let v = positions_only(v);
		assert_eq!(v[0], vec3(1.5, 2.5, 3.0));
		assert_eq!(v[1], vec3(0.5, 2.5, 3.0));
		assert_eq!(v[2], vec3(0.5, 1.5, 3.0));
		assert_eq!(v[3], vec3(1.5, 2.5, 3.0));
		assert_eq!(v[4], vec3(0.5, 1.5, 3.0));
		assert_eq!(v[5], vec3(1.5, 1.5, 3.0));
	}
}
