
use glm::{
	vec3,
	Vec3
};
use crate::{
	engine::{
		TimeDelta,
		Vertex
	},
	geometry::{
		Geometry,
		Rectangle
	},
	material::MaterialId,
	math,
	units::Color
};

#[derive(Debug)]
pub struct RectangularPrism {
	size: Vec3,
	/// The location of the prism. How the prism orients
	/// around the origin is controlled by the anchor.
	origin: Vec3,
	/// The anchor is the location on the prism that sits at the origin.
	/// For example, an anchor of (0, -1, 0) would cause the prism to be
	/// standing upright with the bottom centered at the origin.
	anchor: Vec3,
	rotation: Vec3,
	color: Color,
	material: MaterialId
}

impl RectangularPrism {
	// 6 faces
	pub const VERTEX_COUNT: usize = 6 * Rectangle::VERTEX_COUNT;

	pub fn new(size: Vec3, origin: Vec3, anchor: Vec3, rotation: Vec3, color: Color, material: MaterialId) -> Self {
		assert!((-1.0..=1.0).contains(&anchor.x));
		assert!((-1.0..=1.0).contains(&anchor.y));
		assert!((-1.0..=1.0).contains(&anchor.z));
		Self {
			size,
			origin,
			anchor,
			rotation,
			color,
			material
		}
	}
}

impl Geometry for RectangularPrism {
	fn vertex_count(&self) -> usize {
		Self::VERTEX_COUNT
	}

	fn material(&self) -> MaterialId {
		self.material
	}

	fn update(&self, vertices: &mut [Vertex], _time: &TimeDelta) {
		assert_eq!(vertices.len(), self.vertex_count());
		let center = self.origin + self.size / 2.0 * -self.anchor;

		// names are in xyz order to ease thinking about +-1
		let radius = self.size / 2.0;
		let     left_top_front = center + radius * vec3(-1.0,  1.0,  1.0);
		let      left_top_back = center + radius * vec3(-1.0,  1.0, -1.0);
		let  left_bottom_front = center + radius * vec3(-1.0, -1.0,  1.0);
		let   left_bottom_back = center + radius * vec3(-1.0, -1.0, -1.0);
		let    right_top_front = center + radius * vec3( 1.0,  1.0,  1.0);
		let     right_top_back = center + radius * vec3( 1.0,  1.0, -1.0);
		let right_bottom_front = center + radius * vec3( 1.0, -1.0,  1.0);
		let  right_bottom_back = center + radius * vec3( 1.0, -1.0, -1.0);

		let m = math::rotation_matrix(self.rotation);
		let     left_top_front = math::rotate_around_with(     left_top_front, center, &m);
		let      left_top_back = math::rotate_around_with(      left_top_back, center, &m);
		let  left_bottom_front = math::rotate_around_with(  left_bottom_front, center, &m);
		let   left_bottom_back = math::rotate_around_with(   left_bottom_back, center, &m);
		let    right_top_front = math::rotate_around_with(    right_top_front, center, &m);
		let     right_top_back = math::rotate_around_with(     right_top_back, center, &m);
		let right_bottom_front = math::rotate_around_with( right_bottom_front, center, &m);
		let  right_bottom_back = math::rotate_around_with(  right_bottom_back, center, &m);

		vertices.copy_from_slice(&[
			// front top left
			Vertex::from(   right_top_front, self.color, vec3( 0.0,  0.0,  1.0)),
			Vertex::from(    left_top_front, self.color, vec3( 0.0,  0.0,  1.0)),
			Vertex::from( left_bottom_front, self.color, vec3( 0.0,  0.0,  1.0)),
			// front bottom right
			Vertex::from(   right_top_front, self.color, vec3( 0.0,  0.0,  1.0)),
			Vertex::from( left_bottom_front, self.color, vec3( 0.0,  0.0,  1.0)),
			Vertex::from(right_bottom_front, self.color, vec3( 0.0,  0.0,  1.0)),
			// back top left
			Vertex::from(     left_top_back, self.color, vec3( 0.0,  0.0, -1.0)),
			Vertex::from(    right_top_back, self.color, vec3( 0.0,  0.0, -1.0)),
			Vertex::from( right_bottom_back, self.color, vec3( 0.0,  0.0, -1.0)),
			// back bottom right
			Vertex::from(     left_top_back, self.color, vec3( 0.0,  0.0, -1.0)),
			Vertex::from( right_bottom_back, self.color, vec3( 0.0,  0.0, -1.0)),
			Vertex::from(  left_bottom_back, self.color, vec3( 0.0,  0.0, -1.0)),
			// left top left
			Vertex::from(    left_top_front, self.color, vec3(-1.0,  0.0,  0.0)),
			Vertex::from(     left_top_back, self.color, vec3(-1.0,  0.0,  0.0)),
			Vertex::from(  left_bottom_back, self.color, vec3(-1.0,  0.0,  0.0)),
			// left bottom right
			Vertex::from(    left_top_front, self.color, vec3(-1.0,  0.0,  0.0)),
			Vertex::from(  left_bottom_back, self.color, vec3(-1.0,  0.0,  0.0)),
			Vertex::from( left_bottom_front, self.color, vec3(-1.0,  0.0,  0.0)),
			// right top left
			Vertex::from(    right_top_back, self.color, vec3( 1.0,  0.0,  0.0)),
			Vertex::from(   right_top_front, self.color, vec3( 1.0,  0.0,  0.0)),
			Vertex::from(right_bottom_front, self.color, vec3( 1.0,  0.0,  0.0)),
			// right bottom right
			Vertex::from(    right_top_back, self.color, vec3( 1.0,  0.0,  0.0)),
			Vertex::from(right_bottom_front, self.color, vec3( 1.0,  0.0,  0.0)),
			Vertex::from( right_bottom_back, self.color, vec3( 1.0,  0.0,  0.0)),
			// top top left
			Vertex::from(    right_top_back, self.color, vec3( 0.0,  1.0,  0.0)),
			Vertex::from(     left_top_back, self.color, vec3( 0.0,  1.0,  0.0)),
			Vertex::from(    left_top_front, self.color, vec3( 0.0,  1.0,  0.0)),
			// top bottom right
			Vertex::from(    right_top_back, self.color, vec3( 0.0,  1.0,  0.0)),
			Vertex::from(    left_top_front, self.color, vec3( 0.0,  1.0,  0.0)),
			Vertex::from(   right_top_front, self.color, vec3( 0.0,  1.0,  0.0)),
			// bottom top left
			Vertex::from(right_bottom_front, self.color, vec3( 0.0, -1.0,  0.0)),
			Vertex::from( left_bottom_front, self.color, vec3( 0.0, -1.0,  0.0)),
			Vertex::from(  left_bottom_back, self.color, vec3( 0.0, -1.0,  0.0)),
			// bottom bottom right
			Vertex::from(right_bottom_front, self.color, vec3( 0.0, -1.0,  0.0)),
			Vertex::from(  left_bottom_back, self.color, vec3( 0.0, -1.0,  0.0)),
			Vertex::from( right_bottom_back, self.color, vec3( 0.0, -1.0,  0.0)),
		]);
	}
}
