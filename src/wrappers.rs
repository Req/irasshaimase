
use glm::Vec3;
use wasm_bindgen::{
	JsCast,
	JsValue
};

#[allow(unused_macros)]
macro_rules! log {
	($($t:tt)*) => {
		web_sys::console::log_1(&format!($($t)*).into())
	}
}

pub fn by_id<T>(id: &str) -> Option<T> where T: JsCast {
	web_sys::window().unwrap().document().unwrap().get_element_by_id(id).map(|e| e.dyn_into::<T>().unwrap())
}

pub trait WebExpect<T> {
	fn expect_log(self, s: &str) -> T;
	fn expect_log_f<F>(self, f: F) -> T where F: FnOnce() -> String;
}

impl<T> WebExpect<T> for Result<T, JsValue> {
	fn expect_log(self, s: &str) -> T {
		match self {
			Ok(value) => value,
			Err(_) => {
				log!("(panicking) {}: got JS error", s);
				panic!();
			}
		}
	}

	fn expect_log_f<F>(self, f: F) -> T where F: FnOnce() -> String {
		match self {
			Ok(value) => value,
			Err(_) => {
				log!("(panicking) {}: got JS error", f());
				panic!();
			}
		}
	}
}

impl<T> WebExpect<T> for Result<T, String> {
	fn expect_log(self, s: &str) -> T {
		match self {
			Ok(value) => value,
			Err(e) => {
				log!("(panicking) {}: {}", e, s);
				panic!();
			}
		}
	}

	fn expect_log_f<F>(self, f: F) -> T where F: FnOnce() -> String {
		match self {
			Ok(value) => value,
			Err(e) => {
				log!("(panicking) {}: {}", e, f());
				panic!();
			}
		}
	}
}

impl<T> WebExpect<T> for Option<T> {
	fn expect_log(self, s: &str) -> T {
		match self {
			Some(value) => value,
			None => {
				log!("(panicking) {}: got None", s);
				panic!();
			}
		}
	}

	fn expect_log_f<F>(self, f: F) -> T where F: FnOnce() -> String {
		match self {
			Some(value) => value,
			None => {
				log!("(panicking) {}: got None", f());
				panic!();
			}
		}
	}
}

pub const fn const_vec3(x: f32, y: f32, z: f32) -> Vec3 {
	Vec3 {
		x,
		y,
		z
	}
}

pub const fn const_vec1(v: f32) -> Vec3 {
	const_vec3(v, v, v)
}
