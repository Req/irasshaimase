
use crate::{
	engine::GL,
	shaders::basic::BasicShader
};

#[derive(Clone, Debug, Default)]
pub struct DirectionalLight {
	pub direction: [f32; 3],

	pub color: [f32; 3],
	pub ambient: f32,
	pub diffuse: f32,
	pub specular: f32
}

impl DirectionalLight {
	pub fn uniform(&self, gl: &GL, shader: &BasicShader) {
		gl.uniform3fv_with_f32_array(Some(&shader.uniforms.directional_light.direction), &self.direction);
		gl.uniform3fv_with_f32_array(Some(&shader.uniforms.directional_light.color), &self.color);
		gl.uniform1f(Some(&shader.uniforms.directional_light.ambient), self.ambient);
		gl.uniform1f(Some(&shader.uniforms.directional_light.diffuse), self.diffuse);
		gl.uniform1f(Some(&shader.uniforms.directional_light.specular), self.specular);
	}
}

#[derive(Clone, Debug, Default)]
pub struct PointLight {
	pub position: [f32; 3],

	pub color: [f32; 3],
	pub ambient: f32,
	pub diffuse: f32,
	pub specular: f32,

	pub constant: f32,
	pub linear: f32,
	pub quadratic: f32
}

impl PointLight {
	pub fn uniform(&self, gl: &GL, shader: &BasicShader, index: usize) {
		gl.uniform3fv_with_f32_array(Some(&shader.uniforms.point_lights[index].position), &self.position);
		gl.uniform3fv_with_f32_array(Some(&shader.uniforms.point_lights[index].color), &self.color);
		gl.uniform1f(Some(&shader.uniforms.point_lights[index].ambient), self.ambient);
		gl.uniform1f(Some(&shader.uniforms.point_lights[index].diffuse), self.diffuse);
		gl.uniform1f(Some(&shader.uniforms.point_lights[index].specular), self.specular);
		gl.uniform1f(Some(&shader.uniforms.point_lights[index].constant), self.constant);
		gl.uniform1f(Some(&shader.uniforms.point_lights[index].linear), self.linear);
		gl.uniform1f(Some(&shader.uniforms.point_lights[index].quadratic), self.quadratic);
	}
}

#[derive(Clone, Debug, Default)]
pub struct SpotLight {
	pub position: [f32; 3],
	pub direction: [f32; 3],
	pub inner_cutoff: f32,
	pub outer_cutoff: f32,

	pub color: [f32; 3],
	pub ambient: f32,
	pub diffuse: f32,
	pub specular: f32
}

impl SpotLight {
	pub fn uniform(&self, gl: &GL, shader: &BasicShader, index: usize) {
		gl.uniform3fv_with_f32_array(Some(&shader.uniforms.spot_lights[index].position), &self.position);
		gl.uniform3fv_with_f32_array(Some(&shader.uniforms.spot_lights[index].direction), &self.direction);
		gl.uniform3fv_with_f32_array(Some(&shader.uniforms.spot_lights[index].color), &self.color);
		gl.uniform1f(Some(&shader.uniforms.spot_lights[index].inner_cutoff), self.inner_cutoff);
		gl.uniform1f(Some(&shader.uniforms.spot_lights[index].outer_cutoff), self.outer_cutoff);
		gl.uniform1f(Some(&shader.uniforms.spot_lights[index].ambient), self.ambient);
		gl.uniform1f(Some(&shader.uniforms.spot_lights[index].diffuse), self.diffuse);
		gl.uniform1f(Some(&shader.uniforms.spot_lights[index].specular), self.specular);
	}
}

#[allow(unused)]
#[derive(Debug)]
pub enum Light {
	Point(PointLight),
	Spot(SpotLight)
}
