
use std::f32::consts::PI;
use glm::{
	Mat4,
	vec3,
	Vec3
};

pub struct Camera {
	horizontal_angle: f32,
	vertical_angle: f32,
	ratio: f32,
	position: Vec3
}

impl Camera {
	pub fn viewport(&mut self, width: u32, height: u32) {
		self.ratio = width as f32 / height as f32;

		/*
		 * At a 0.5 ratio, the camera should be centered.
		 * At a 1.5 ratio, the camera should be maximally offset.
		 */
		const OFFSET: f32 = 3.0;
		let offset_ratio = self.ratio.clamp(0.5, 1.5) - 0.5;
		self.position = vec3(offset_ratio * OFFSET, 2.0, 3.0);
	}

	pub fn perspective(&self) -> Mat4 {
		glm::ext::perspective(PI / 2.0, self.ratio, 0.1, 100.0)
	}

	pub fn new() -> Self {
		Self {
			horizontal_angle: 0.0,
			vertical_angle: 0.0,
			ratio: 0.0,
			position: vec3(2.0, 2.0, 3.0)
		}
	}

	pub fn position(&self) -> &Vec3 {
		&self.position
	}

	pub fn view(&self) -> Mat4 {
		let right = vec3(
			(self.horizontal_angle - PI / 2.0).sin(),
			0.0,
			(self.horizontal_angle - PI / 2.0).cos()
		);
		let direction = vec3(
			self.vertical_angle.cos() * self.horizontal_angle.sin(),
			self.vertical_angle.sin(),
			self.vertical_angle.cos() * self.horizontal_angle.cos()
		);
		let up = glm::cross(right, direction);
		glm::ext::look_at(
			self.position,
			self.position - direction,
			up
		)
	}
}
