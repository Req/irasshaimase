
use glm::Vec3;
use crate::wrappers::const_vec1;

#[repr(u8)]
#[allow(unused)]
#[derive(Clone, Copy, Debug)]
pub enum MaterialId {
	DarkWood,
	LightWood,
	Concrete,
	DarkMetal,
	LightMetal,
	Water,
	Paper
}

pub const MATERIAL_COUNT: usize = 7;

impl MaterialId {
	pub fn from_index(index: usize) -> MaterialId {
		assert!(index < MATERIAL_COUNT);
		let index = index as u8;
		unsafe {
			std::mem::transmute(index)
		}
	}

	pub fn index(&self) -> usize {
		*self as usize
	}

	pub fn material(&self) -> &'static Material {
		&Material::MATERIALS[self.index()]
	}
}

#[repr(C)]
#[derive(Debug)]
pub struct Material {
	pub ambient: Vec3,
	pub diffuse: Vec3,
	pub specular: Vec3,
	pub shininess: f32
}

impl Material {
	const MATERIALS: [Material; MATERIAL_COUNT] = [
		DARK_WOOD,
		LIGHT_WOOD,
		CONCRETE,
		DARK_METAL,
		LIGHT_METAL,
		WATER,
		PAPER
	];
}

const DARK_WOOD: Material = Material {
	ambient: const_vec1(0.4),
	diffuse: const_vec1(0.7),
	specular: const_vec1(0.8),
	shininess: 20.0
};

const LIGHT_WOOD: Material = Material {
	ambient: const_vec1(0.5),
	diffuse: const_vec1(0.9),
	specular: const_vec1(0.5),
	shininess: 10.0
};

const CONCRETE: Material = Material {
	ambient: const_vec1(0.5),
	diffuse: const_vec1(0.7),
	specular: const_vec1(0.3),
	shininess: 5.0
};

const DARK_METAL: Material = Material {
	ambient: const_vec1(0.3),
	diffuse: const_vec1(0.8),
	specular: const_vec1(0.9),
	shininess: 48.0
};

const LIGHT_METAL: Material = Material {
	ambient: const_vec1(0.6),
	diffuse: const_vec1(0.9),
	specular: const_vec1(0.8),
	shininess: 32.0
};

const WATER: Material = Material {
	ambient: const_vec1(0.8),
	diffuse: const_vec1(0.9),
	specular: const_vec1(0.8),
	shininess: 32.0
};

const PAPER: Material = Material {
	ambient: const_vec1(0.6),
	diffuse: const_vec1(0.9),
	specular: const_vec1(0.4),
	shininess: 10.0
};
